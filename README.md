# Docker Build

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://lab.frogg.it/swepy/cicd-templates/docker-build/-/blob/main/LICENSE)

## Objective

Build a Docker image and push it to the GitLab registry.

## How to use it

### Include the component

Add the following to your `.gitlab-ci.yml` file.

```yaml
include:
  - component: $CI_SERVER_FQDN/swepy/cicd-templates/docker-build/docker-build@0.3.0
```

[![Supported by GitLab.com](https://img.shields.io/badge/Supported_by-GitLab.com-orange)](https://gitlab.com)
[![Supported by Frogg.it](https://img.shields.io/badge/Supported_by-Frogg.it-green)](https://froggit.fr/)

### Inputs

You can customize the job by overriding specific inputs.

| Name              | Description                                              | Default                             |
|-------------------|----------------------------------------------------------|-------------------------------------|
| `stage`           | Stage of the job.                                        | `test`                              |
| `image_id`        | Image name and tag of the built image.                   | `$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA` |
| `context_path`    | Path to the build context, usually the project.          | `.`                                 |
| `dockerfile_name` | Name of the dockerfile.                                  | `Dockerfile`                        |
| `docker_host`     | Docker daemon host URL.                                  | `tcp://docker:2375/`                |
| `docker_driver`   | Storage driver used by Docker.                           | `overlay2`                          |
| `kaniko_options`  | Additional options to pass to Kaniko for image building. | `""`                                |
| `kaniko_tag`      | Version tag of the Kaniko executor image.                | `v1.23.2-debug`                     |

For example:

```yml
include:
  - component: $CI_SERVER_FQDN/swepy/cicd-templates/docker-build/docker-build@0.3.0
    inputs:
      stage: docker_build
      context_path: ./project
```

## Variables

You can customize the job by overriding the following variables:

| Name              | Description                                              | Default                         |
|-------------------|----------------------------------------------------------|---------------------------------|
| `IMAGE_ID`        | Image name and tag of the built image.                   | `$[[ inputs.image_id ]]`        |
| `CONTEXT_PATH`    | Path to the build context, usually the project.          | `$[[ inputs.context_path ]]`    |
| `DOCKERFILE_NAME` | Name of the dockerfile.                                  | `$[[ inputs.dockerfile_name ]]` |
| `DOCKER_HOST`     | Docker daemon host URL.                                  | `$[[ inputs.docker_host ]]`     |
| `DOCKER_DRIVER`   | Storage driver used by Docker.                           | `$[[ inputs.docker_driver ]]`   |
| `KANIKO_OPTIONS`  | Additional options to pass to Kaniko for image building. | `$[[ inputs.kaniko_options ]]`  |
| `KANIKO_TAG`      | Version tag of the Kaniko executor image.                | `$[[ inputs.kaniko_tag ]]`      |
