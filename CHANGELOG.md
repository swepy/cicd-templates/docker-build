# Changelog

All notable changes to this job will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2025-03-01

### Added

* Support [inputs](https://docs.gitlab.com/ci/yaml/inputs/):
  * stage: Stage of the job
  * image_id: Image name and tag of the built image
  * context_path: Path to the build context, usually the project
  * dockerfile_name: Name of the dockerfile
  * docker_host: Docker daemon host URL
  * docker_driver: Storage driver used by Docker
  * kaniko_options: Additional options to pass to Kaniko for image building
  * kaniko_tag: Version tag of the Kaniko executor image
* Variables:
  * DOCKERFILE_NAME
  * KANIKO_TAG

### Changed

* Variables:
  * TARGET_IMAGE -> IMAGE_ID
  * PROJECT_PATH -> CONTEXT_PATH

## [0.2.1] - 2024-07-16

* Update CICD

## [0.2.0] - 2024-06-24

### Added

* a new variable to inject kaniko's options to the command: `KANIKO_OPTIONS`

### Changed

* `docker-build` now rely on kaniko to build images

### Removed

* `TAG` and `IMAGE_NAME` variables, `TARGET_IMAGE` now rely directly on 
  `CI_REGISTRY_IMAGE` and `CI_COMMIT_SHA`

## [0.1.1] - 2024-06-01

### Fixed

* Fixed link to the template in documentation README.md

## [0.1.0] - 2024-06-01

* Initial version
